# Command Center

## What does it do
It is currently just creating infrastructure to save transactions into S3. The idea is that S3 is the source of truth 
and this can be extended to trigger processing on object creation. That will come in a future iteration.

![alt text](assets/CCArchitecture.png "Command Center Architecture")

## Security
* The Lambda is secured by an API Key on the API Gateway to prevent undesirable requests. There is no egress of transactions through the gateway.
* Transactions are stored in an encrypted S3 bucket
* The Lambda only has write permissions to the bucket

## Requirements
* [Terraform 0.12 or later](https://github.com/hashicorp/terraform/releases)
* [An AWS Account](https://aws.amazon.com/free/)
  * This is using free-tier resources, but I would recommend reading over their free tier to ensure you are not suprised
    * API Gateway is free within the first 12 months, but it would cost very little after that given low transaction volume
* [AWS credentials configured in your environment](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)
* Terraform state is stored locally which contains the API keys. If you want this more secure, you can implement [remote encrypted storage](https://www.terraform.io/docs/backends/types/s3.html#using-the-s3-remote-state)

## Usage
* Make your own copy of `installs/sample-command-center` in the same directory
* Navigate into your command center directory
* Update the local variables in `main.tf`
* `terraform init`
* `terraform plan`
* Verify you are happy with all the resources that are going to be created in your account. There may be billing implications, especially if you have an existing account that is no longer within the free tier.
* `terraform apply` and enter `yes`
* Get your personal test key: `terraform show -json | jq '.values.root_module.resources[] | select(.address == "aws_api_gateway_api_key.test_key") | .values.value'`
* Get your investec key: `terraform show -json | jq '.values.root_module.resources[] | select(.address == "aws_api_gateway_api_key.investec_key") | .values.value'`
> Terraform seems to refuse to print out secrets which is why I'm interrogating the state directly to get the API keys
* The api endpoint should be printed out
* Invoke it adding your api key as a header `x-api-key` and a sample transaction payload
> The payload is parsed as JSON and the `dateTime` parameter is used so you can't use any payload

## Future Work
* Queryable transactions (e.g. somehow using Athena)
* Visualization of transactions
* AWS Config to define transaction accept/deny parameters
* CI/CD on Lambda
