locals {
  aws_account_id = "<your aws account id>"
  aws_region = "<your preferred aws region, e.g. eu-west-1>"
  transactions_bucket = "<a name for an S3 bucket that will be created for your transactions>"
}

module "transaction_handler" {
  source = "../../modules/transaction-handler"
  aws_account_id = local.aws_account_id
  aws_region = local.aws_region
  destination_bucket_name = local.transactions_bucket
}

# Key to be added to env.json in Programmable Banking
resource "aws_api_gateway_api_key" "investec_key" {
  name = "investec_key"
}

# Personal testing key
resource "aws_api_gateway_api_key" "test_key" {
  name = "test_key"
}

resource "aws_api_gateway_usage_plan_key" "investec_key" {
  key_id = aws_api_gateway_api_key.investec_key.id
  key_type = "API_KEY"
  usage_plan_id = module.transaction_handler.standard_usage_plan.id
}

resource "aws_api_gateway_usage_plan_key" "test_key" {
  key_id = aws_api_gateway_api_key.test_key.id
  key_type = "API_KEY"
  usage_plan_id = module.transaction_handler.standard_usage_plan.id
}
