# API Gateway
resource "aws_api_gateway_rest_api" "card_transaction_api" {
  name = "transaction-api"
}

resource "aws_api_gateway_resource" "transaction" {
  path_part   = "transaction"
  parent_id   = aws_api_gateway_rest_api.card_transaction_api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.card_transaction_api.id
}

resource "aws_api_gateway_resource" "card_transaction" {
  path_part   = "card"
  parent_id   = aws_api_gateway_resource.transaction.id
  rest_api_id = aws_api_gateway_rest_api.card_transaction_api.id
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.card_transaction_api.id
  resource_id   = aws_api_gateway_resource.card_transaction.id
  http_method   = "POST"
  authorization = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.card_transaction_api.id
  resource_id             = aws_api_gateway_resource.card_transaction.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.ingress_lambda.invoke_arn
}

resource "aws_api_gateway_deployment" "prod" {
  depends_on  = [aws_api_gateway_integration.integration]
  rest_api_id = aws_api_gateway_rest_api.card_transaction_api.id
  stage_name  = "prod"
}

resource "aws_api_gateway_usage_plan" "standard" {
  name         = "standard"
  description  = "Represents the standard usage plan"
  product_code = "STANDARD"

  api_stages {
    api_id = aws_api_gateway_rest_api.card_transaction_api.id
    stage  = aws_api_gateway_deployment.prod.stage_name
  }
}

# Lambda
resource "aws_lambda_permission" "apigw_ingress_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ingress_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.aws_region}:${var.aws_account_id}:${aws_api_gateway_rest_api.card_transaction_api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.card_transaction.path}"
}

resource "aws_lambda_function" "ingress_lambda" {
  filename      = "${path.module}/ingress_lambda.zip"
  function_name = "handleCardTransaction"
  role          = aws_iam_role.ingress_lambda.arn
  handler       = "ingress_lambda.handle_card_transaction"
  runtime       = "python3.7"
  environment {
    variables = {
      destination_bucket = var.destination_bucket_name
    }
  }

  source_code_hash = filebase64sha256("${path.module}/ingress_lambda.zip")
}

# IAM
resource "aws_iam_role" "ingress_lambda" {
  name = "IngressLambdaRole"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role = aws_iam_role.ingress_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "object_storage" {
  role = aws_iam_role.ingress_lambda.name
  policy_arn = aws_iam_policy.create_transaction_object.arn
}
