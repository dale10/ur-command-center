import boto3
import json
import os
from datetime import datetime

s3_client = boto3.client('s3')
destination_bucket = os.environ["destination_bucket"]

def handle_card_transaction(event, context):
    payload = json.loads(event["body"])
    transaction_date = datetime.fromisoformat(payload["dateTime"].replace("Z", "+00:00"))
    object_key = "card/{}/{}/{}.json".format(transaction_date.year, transaction_date.month, payload["dateTime"])
    s3_client.put_object(Body=event["body"], Bucket=destination_bucket, Key=object_key)
    return {
        'statusCode': 200,
    }

