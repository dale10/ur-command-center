output "rest_api" {
  value = aws_api_gateway_rest_api.card_transaction_api
}

output "deployment" {
  value = aws_api_gateway_deployment.prod
}

output "standard_usage_plan" {
  value = aws_api_gateway_usage_plan.standard
}

output "api_card_transaction_endpoint" {
  value = "${aws_api_gateway_deployment.prod.invoke_url}${aws_api_gateway_resource.card_transaction.path}"
}

