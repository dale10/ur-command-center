resource "aws_s3_bucket" "transactions" {
  bucket = var.destination_bucket_name
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

## Block public access to result extract bucket
resource "aws_s3_bucket_public_access_block" "result_extract_bucket_block_public_access" {
  bucket                  = aws_s3_bucket.transactions.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_iam_policy" "create_transaction_object" {
  name = "CreateTransactionObject"
  path = "/"
  description = "IAM policy that allows the creation of transaction objects in S3"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${var.destination_bucket_name}/*"
        }
    ]
}
POLICY
}

