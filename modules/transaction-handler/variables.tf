variable "aws_account_id" {
  type = string
  description = "The AWS accountId of the aws provider"
}

variable "aws_region" {
  type = string
  description = "The default AWS region of the aws provider"
}

variable "destination_bucket_name" {
  type = string
}
